<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/
css/bootstrap.min.css">
</head>
<body>
  <div class="container">
    <form role="form" method="post">

      <input type="hidden" name="student_id" id="student_id">

      <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" required>
      </div>

      <div class="form-group">
        <label for="address">Residential Address:</label>
        <textarea rows="4" name="address" class="form-control" id="address" placeholder="Enter address" required></textarea>
      </div>

      <div class="form-group">
        <label for="gender">Gender:</label>
        <div class="radio">
          <label><input type="radio" name="gender" value="male"> Male</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="gender" value="female"> Female</label>
        </div>
      </div>

      <div class="form-group">
        <label for="pass_year">Expected year of passing:</label>
        <select name="pass_year" class="form-control" id="pass_year">
          <option value="2015">2015</option>
          <option value="2016">2016</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
        </select>
      </div>

      <div class="form-group">
        <label for="interests">Extra Curricular Interests</label>
        <div class="checkbox">
          <label><input name="interests_sports" type="checkbox" value="sports">Sports</label>
        </div>
        <div class="checkbox">
          <label><input name="interests_programming" type="checkbox" value="programming">Prgramming</label>
        </div>
        <div class="checkbox">
          <label><input name="interests_arts" type="checkbox" value="arts">Arts</label>
        </div>
        <div class="checkbox">
          <label><input name="interests_music" type="checkbox" value="music">Music</label>
        </div>
      </div>

      <div class="form-group">
        <input type="submit" class="btn btn-default" value="Submit">
      </div>

    </form>
  </div>
</body>
