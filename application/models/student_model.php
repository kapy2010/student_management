<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {

  public function display()
  {
    $query = $this->db->get('student');
    return $query->result();
  }

  public function getRow($id)
  {
    $query = $this->db->get_where('student', array('id' => $id));
    return $query->result();
  }

  public function create($row)
  {
    $data = array(
      'name' => $row['name'],
      'address' => $row['address'],
      'gender' => $row['gender'],
      'pass_year' => $row['pass_year']
    );

    $this->db->insert('student', $data);
  }

  public function update($row)
  {
    $data = array(
      'name' => $row['name'],
      'address' => $row['address'],
      'gender' => $row['gender'],
      'pass_year' => $row['pass_year']
    );

    $this->db->set('student', $data);
  }

  public function delete($id)
  {
    $this->db->delete('student', array('id' => $id));

  }
}

?>
