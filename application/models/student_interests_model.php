<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_interests_model extends CI_Model {

  public function getData($id)
  {
    $query = $this->db->get_where('student_interests', array('student_id' => $id));
    return $query->result();
  }

  public function create($row)
  {
    $name = $row["name"];
    $sql = "SELECT id from student WHERE name='$name'";
    $query = $this->db->query($sql);
    $result = $query->result();
    $student_id = $result[0]->id;

    $interests = array();
    if (array_key_exists('interests_sports', $row)) { array_push($interests, "sports"); }
    if (array_key_exists('interests_programming', $row)) { array_push($interests, "programming"); }
    if (array_key_exists('interests_arts', $row)) { array_push($interests, "arts"); }
    if (array_key_exists('interests_music', $row)) { array_push($interests, "music"); }

    foreach($interests as $interest) {
      $sql = "INSERT INTO student_interests (student_id, interest)
        VALUES ('$student_id', '$interest')";
      $this->db->query($sql);
    }

  }

  public function update($row)
  {
    $student_id = $row["student_id"];
    $interests = array();
    if (array_key_exists('interests_sports', $row)) { array_push($interests, "sports"); }
    if (array_key_exists('interests_programming', $row)) { array_push($interests, "programming"); }
    if (array_key_exists('interests_arts', $row)) { array_push($interests, "arts"); }
    if (array_key_exists('interests_music', $row)) { array_push($interests, "music"); }

    $old_interests = array();
    $sql = "SELECT interest FROM student_interests WHERE student_id=$student_id";
    $query = $this->db->query($sql);
    $result = $query->result();
    foreach ($result as $row) {
      array_push($old_interests, $row->interest);
    }

    $interests = array_diff($interests, $old_interests);

    foreach($interests as $interest) {
      $sql = "INSERT INTO student_interests (student_id, interest)
        VALUES ('$student_id', '$interest')";
      $this->db->query($sql);
    }
  }

  public function delete($id)
  {
    $this->db->delete('student_interests', array('student_id' => $id));
  }
}
?>
