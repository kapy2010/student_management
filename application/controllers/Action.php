<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->view('header');
  }

  public function index()
  {
    $this->load->model('student_model');
    $result = $this->student_model->display();
    $data = array(
      'result' => $result
    );
    $this->load->view('main', $data);
  }

  public function create()
  {
    $this->load->view('new_form');
    if ($_POST) {
      $this->load->model('student_model');
      $this->load->model('student_interests_model');
      $this->student_model->create($_POST);
      $this->student_interests_model->create($_POST);
      header('Location: http://localhost/ci/index.php/action/');
    }
  }

  public function update($id)
  {
    $this->load->model('student_model');
    $this->load->model('student_interests_model');

    if ($_POST) {
      $this->student_model->update($_POST);
      $this->student_interests_model->update($_POST);
      header('Location: http://localhost/ci/index.php/action/');
    }

    $student_result = $this->student_model->getRow($id);
    $student_interests_result = $this->student_interests_model->getData($id);

    $interests = array();
    foreach ($student_interests_result as $row) {
      array_push($interests, $row->interest);
    }
    $data = array(
      'row' => $student_result[0],
      'interests' => $interests
    );
    $this->load->view('edit_form', $data);
  }

  public function delete($id)
  {
    $this->load->model('student_model');
    $this->load->model('student_interests_model');
    $this->student_interests_model->delete($id);
    $this->student_model->delete($id);
    header('Location: http://localhost/ci/index.php/action/');
  }
}
?>
